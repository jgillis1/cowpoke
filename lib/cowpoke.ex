defmodule Cowpoke do
  @moduledoc """
  Documentation for Cowpoke.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Cowpoke.hello()
      :world

  """
  def hello do
    :world
  end
end
